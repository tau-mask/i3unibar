#pragma once

#include <memory>
#include <vector>

#include "value_collector_options.h"


template<class T>
struct ValueCollectorInterface
{
    // Tracking of previous values

    using VCIShared = std::shared_ptr<T>;

    void setPrevious(VCIShared previous) {
        previous->m_previous.reset();
        m_previous = previous;
    }

    VCIShared m_previous;

    static VCIShared getInstance() {
        return std::make_shared<T>();
    }

    // Interface

    virtual ~ValueCollectorInterface() {}

    virtual void prepareCpuUsage() = 0;
    virtual float getCpuUsage(int cpuNumber) const = 0;
    virtual std::vector<float> getAllCpuUsage() const = 0;

    virtual void prepareMemUsage() = 0;
    virtual uint64_t getMemTotal() = 0;
    virtual uint64_t getMemUsed() = 0;
    virtual uint64_t getMemBuffCache() = 0;

    virtual void prepareNetUsage(
        const std::string& dev,
        vco::NetDetail details
    ) = 0;
    virtual const std::string& getDevIpAddr(const std::string& dev) const = 0;
    virtual float getDevLinkQuality(const std::string& dev) const = 0;

    virtual void preparePartUsage(const std::string& path) = 0;
    virtual uint64_t getPartUsed(const std::string& path) const = 0;
    virtual uint64_t getPartTotal(const std::string& path) const = 0;
};


#if defined(__linux__)
    #include "linux/value_collector.h"
#endif
