#pragma once


namespace vco {


enum NetDetail : int {
    None        = 0x0,
    Wifi        = 0x1,
};


} // vco
