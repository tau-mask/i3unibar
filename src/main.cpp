#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>
#include <string>

#include "CLI11.hpp"
#include "json.hpp"
#include "renderers/cpu.h"
#include "renderers/mem.h"
#include "renderers/net.h"
#include "renderers/part.h"
#include "value_collector_if.h"


int main(int argc, char **argv)
{
    // Parse the command line
    CLI::App app{"App description"};

    std::string configPath = "i3unibar.json";
    app.add_option("-c,--config", configPath, "Path for a configuration file");

    CLI11_PARSE(app, argc, argv);

    // Read the configuration file
    nlohmann::json conf;
    {
        std::ifstream in(configPath);
        if(in.fail()) {
            std::cerr << "Cannot find configuration file" << std::endl;
            return 1;
        }
        in >> conf;
    }
    if(conf.at("version") != 1)
        std::cerr << "Unknown version, the program might crash" << std::endl;

    std::vector<rend::UniqueRenderer> renderers;

    if(conf.contains("cpu")) {
        auto confCpu = conf["cpu"];
        auto cpuRenderer = std::make_unique<rend::CpuRenderer>(confCpu);
        renderers.push_back(std::move(cpuRenderer));
    }

    if(conf.contains("mem")) {
        auto confMem = conf["mem"];
        auto memRenderer = std::make_unique<rend::MemRenderer>(confMem);
        renderers.push_back(std::move(memRenderer));
    }

    if(conf.contains("part")) {
        for(auto& confPart: conf["part"]) {
            auto partRenderer = std::make_unique<rend::PartRenderer>(confPart);
            renderers.push_back(std::move(partRenderer));
        }
    }

    if(conf.contains("net")) {
        for(auto& confNet: conf["net"]) {
            auto renderer = std::make_unique<rend::NetRenderer>(confNet);
            renderers.push_back(std::move(renderer));
        }
    }

    auto oldValueCollector = ValueCollector::getInstance();

    std::cout << "{\"version\":1}[";

    using ttime = decltype(std::chrono::steady_clock::now());
    std::vector<ttime> times;
    auto durationCast = [](const ttime& t1, const ttime& t2) -> auto {
        using us = std::chrono::microseconds;
        return std::chrono::duration_cast<us>(t2 - t1).count();
    };

    while(true) {
        times.clear();
        times.push_back(std::chrono::steady_clock::now());

        auto valueCollector = ValueCollector::getInstance();
        valueCollector->setPrevious(oldValueCollector);

        times.push_back(std::chrono::steady_clock::now());

        std::cout << '[';
        for(std::size_t i = 0; i < renderers.size(); ++i) {
            std::cout << rend::InfoPrinter(*renderers[i] , *valueCollector);
            if(i < renderers.size() - 1)
                std::cout << ',';
            times.push_back(std::chrono::steady_clock::now());
        }
        std::cout << "],";

        oldValueCollector = valueCollector;

        times.push_back(std::chrono::steady_clock::now());

        std::cerr << "timings: ";
        auto ntimes = times.size();
        for(std::size_t i = 0; i + 1 < ntimes; ++i)
            std::cerr << durationCast(times[i], times[i+1]) << ",";
        if(ntimes > 0)
            std::cerr << durationCast(times[0], times[ntimes-1]) << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    return 0;
}
