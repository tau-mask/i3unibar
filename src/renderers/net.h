#pragma once

#include "../json.hpp"

#include "../renderer.h"


namespace rend {


class NetRenderer: public InfoRenderer
{
public:
    NetRenderer(const nlohmann::json& conf);
    virtual ~NetRenderer() {}

private:
    void render(
        std::string& out,
        ValueCollector& valueCollector
    ) const final;

    std::string m_dev;
    std::vector<std::string> m_fields;
    bool m_isWifi;
};


} // rend
