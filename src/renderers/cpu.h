#pragma once

#include "../json.hpp"

#include "../renderer.h"

namespace rend {


class CpuRenderer: public InfoRenderer
{
public:
    CpuRenderer(const nlohmann::json& conf);
    virtual ~CpuRenderer() {}

private:
    void render(
        std::string& out,
        ValueCollector& valueCollector
    ) const final;

    const std::vector<int> m_listIndexCpus;
};


} // rend
