#include "part.h"

#include "../util/convert.h"


namespace rend {


PartRenderer::PartRenderer(const nlohmann::json& conf) :
    InfoRenderer(false),
    m_path(conf["path"].get<std::string>())
{
}

void PartRenderer::render(std::string& out, ValueCollector& valueCollector) const
{
    valueCollector.preparePartUsage(m_path);
    uint64_t diskUsed = valueCollector.getPartUsed(m_path);
    uint64_t diskTotal = valueCollector.getPartTotal(m_path);

    out += m_path + " ";
    util::toClosestUnit(out, diskUsed);

    out += " ⁄ ";
    util::toClosestUnit(out, diskTotal);
}



} // rend
