#include "cpu.h"

#include <vector>

#include "../to_utf8/vertical_bar.h"


namespace rend {


CpuRenderer::CpuRenderer(const nlohmann::json& conf) :
    InfoRenderer(true),
    m_listIndexCpus(conf["list"].get<std::vector<int>>())
{
}

void CpuRenderer::render(
    std::string& out,
    ValueCollector& valueCollector
) const
{
    out += "CPU ";

    valueCollector.prepareCpuUsage();

    for(int i: m_listIndexCpus) {
        if(i != -1) {
            float perc = valueCollector.getCpuUsage(i);
            out += " ";
            to_utf8::toVerticalBar(out, perc);
        }
        else {
            std::vector<float> percs = valueCollector.getAllCpuUsage();
            for(auto perc: percs) {
                out += " ";
                to_utf8::toVerticalBar(out, perc);
            }
        }
    }
}


} // rend
