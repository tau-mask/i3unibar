#pragma once

#include "../json.hpp"

#include "../renderer.h"


namespace rend {


class PartRenderer: public InfoRenderer
{
public:
    PartRenderer(const nlohmann::json& conf);
    virtual ~PartRenderer() {}

private:
    void render(
        std::string& out,
        ValueCollector& valueCollector
    ) const final;

    std::string m_path;
};


} // rend
