#include "mem.h"

#include <vector>

#include "../to_utf8/horizontal_bar.h"


namespace rend {


MemRenderer::MemRenderer(const nlohmann::json& conf) :
    InfoRenderer(true),
    m_width(conf["width"].get<int>())
{
}

void MemRenderer::render(
    std::string& out,
    ValueCollector& valueCollector
) const
{
    out += "Mem ";

    valueCollector.prepareMemUsage();

    std::vector<uint64_t> memValues(3);
    memValues[0] = valueCollector.getMemUsed();
    memValues[1] = valueCollector.getMemBuffCache();
    memValues[2] = valueCollector.getMemTotal() - memValues[0] - memValues[1];

    to_utf8::toHorizontalBar(out, memValues, m_width);
}


} // rend
