#include "net.h"

#include "../to_utf8/vertical_bar.h"
#include "../util/convert.h"


namespace rend {


const std::string wifiFields[] = {
    "link_quality",
};

template <class L>
bool isWifi(const L& fields)
{
    for(auto& field: fields)
        for(auto& wfield: wifiFields)
            if(field == wfield)
                return true;

    return false;
}


NetRenderer::NetRenderer(const nlohmann::json& conf)
    : InfoRenderer(false)
    , m_dev(conf["dev"].get<std::string>())
    , m_fields(conf["fields"].get<std::vector<std::string>>())
    , m_isWifi(isWifi(m_fields))
{
}

void NetRenderer::render(
    std::string& out,
    ValueCollector& valueCollector
) const
{
    out += m_dev;

    vco::NetDetail details = vco::NetDetail::None;
    if(m_isWifi)
        details = static_cast<vco::NetDetail>(details | vco::NetDetail::Wifi);

    valueCollector.prepareNetUsage(m_dev, details);

    for(auto& field: m_fields) {
        if(field == "ip")
            out += " " + valueCollector.getDevIpAddr(m_dev);
        if(field == "link_quality") {
            float x = valueCollector.getDevLinkQuality(m_dev);
            out += " ";
            to_utf8::toVerticalBar(out, x);
        }
    }
}


} // rend
