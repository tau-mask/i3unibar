#pragma once

#include "../json.hpp"

#include "../renderer.h"


namespace rend {


class MemRenderer: public InfoRenderer
{
public:
    MemRenderer(const nlohmann::json& conf);
    virtual ~MemRenderer() {}

private:
    void render(
        std::string& out,
        ValueCollector& valueCollector
    ) const final;

    unsigned int m_width;
};


} // rend
