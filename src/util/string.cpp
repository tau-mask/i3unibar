#include "string.h"

std::vector<std::string> util::split(
    const std::string& s,
    char delim,
    bool keepEmpty)
{
    using st = std::string::size_type;

    std::vector<std::string> output;
    st s_end = s.length();

    for(st start = 0, finish = 0; finish < s_end; start = finish + 1)
    {
        if((finish = s.find(delim, start)) == std::string::npos)
            finish = s_end;

        std::string sub(s.substr(start, finish - start));
        if(sub.length() != 0 || keepEmpty)
            output.push_back(sub);
    }

    return output;
}

std::vector<std::string_view> util::splitView(
    const std::string& s,
    char delim,
    bool keepEmpty)
{
    using st = std::string::size_type;

    std::vector<std::string_view> output;
    output.reserve(52);
    st s_end = s.length();

    for(st start = 0, finish = 0; finish < s_end; start = finish + 1)
    {
        if((finish = s.find(delim, start)) == std::string::npos)
            finish = s_end;

        std::string_view sub(s.data() + start, finish - start);
        if(sub.length() != 0 || keepEmpty)
            output.push_back(sub);
    }

    return output;
}
