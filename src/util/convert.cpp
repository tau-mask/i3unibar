#include "convert.h"

#include <array>


namespace util {


const std::array<const std::string, 7> units {
    "", "k", "M", "G", "T", "P", "E"
};

void toClosestUnit(std::string&out, uint64_t value)
{
    int offset = 0;
    while(value >= 1024) {
        value /= 1024;
        ++offset;
    }

    out += std::to_string(value);
    out += units[offset];
}


} // util
