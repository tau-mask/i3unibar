#include "read_file.h"

#include <cstdio>

std::string util::readWholeFile(const std::string& path)
{
    std::FILE* fp = std::fopen(path.c_str(), "rb");

    if(!fp)
        return "";  // TODO error handling

    std::string contents;
    char buffer[4097];
    size_t end;

    while(!feof(fp)) {
        end = std::fread(buffer, 1, 4096, fp);
        contents.append(buffer, end);
    }

    std::fclose(fp);

    return contents;
}
