#pragma once

#include <string>

namespace util
{
    std::string readWholeFile(const std::string& path);
}
