#pragma once

#include <vector>
#include <string>
#include <string_view>


namespace util {


    std::vector<std::string> split(
        const std::string& s,
        char delim,
        bool keepEmpty = false);

    std::vector<std::string_view> splitView(
        const std::string& s,
        char delim,
        bool keepEmpty = false);


} // util
