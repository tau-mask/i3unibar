#pragma once

#include <string>


namespace util {


void toClosestUnit(std::string& out, uint64_t value);


} // util
