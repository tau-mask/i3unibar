#include "horizontal_bar.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <numeric>


namespace to_utf8 {


std::array<std::string, 3> colours { "009f00", "9f9f00", "202020" };

std::array<std::string, 7> partialBar { "▏", "▎", "▍", "▌", "▋", "▊", "▉" };

void toHorizontalBar(
    std::string& out,
    const std::vector<uint64_t>& values,
    unsigned int width)
{
    auto nbElems = values.size();
    if(nbElems < 2)
        return;     // TODO error handling

    std::vector<float> bounds(nbElems+1);
    bounds[0] = 0.0f;
    std::partial_sum(values.begin(), values.end(), bounds.begin()+1);
    for(auto& e: bounds)
        e /= bounds.back();

    struct Candidate {
        std::size_t index;
        float length = 0;
    };
    std::vector<std::vector<Candidate>> charColours;
    {
        float incr = 1.0f / width;
        float min = 0.0f;
        float max = incr;
        std::size_t valIndex = 1;

        auto len = [&bounds](std::size_t idx, float min, float max) -> float {
            return std::min(bounds[idx], max) - std::max(bounds[idx-1], min);
        };

        for(unsigned i = 0; i < width; ++i, min += incr, max += incr) {
            std::vector<Candidate> candidates;

            candidates.push_back({valIndex, len(valIndex, min, max)});
            while(bounds[valIndex] < max && valIndex < nbElems) {
                ++valIndex;
                candidates.push_back({valIndex, len(valIndex, min, max)});
            }

            if(candidates.size() > 2) {
                std::stable_sort(candidates.begin(), candidates.end(),
                    [](const Candidate& a, const Candidate& b) -> bool {
                        return a.length < b.length;
                });
                candidates.resize(2);
            }

            charColours.push_back(candidates);
        }
    }

    int length = 1;
    std::size_t curIndex = 1;

    for(unsigned i = 0; i < width; ++i, ++length) {
        if(charColours[i][0].index != curIndex ||
            (charColours[i].size() == 1 && i == width - 1))
        {
            out += "<span color='#" + colours[curIndex-1] + "'>";
            for(int j = 0; j < length; ++j)
                out += "█";
            out += "</span>";

            curIndex = charColours[i][0].index;
            length = 1;
        }

        if( charColours[i].size() == 2) {
            out += "<span color='#" + colours[curIndex-1] + "' "
               +       "bgcolor='#" + colours[curIndex]   + "'>";
            for(int j = 0; j < length - 1; ++j)
                out += "█";
            auto blockIndex = floorf(
                charColours[i][0].length * 8 /
                (charColours[i][0].length + charColours[i][1].length));
            if(blockIndex >= 7)
                blockIndex = 6;
            out += partialBar[blockIndex];
            out += "</span>";

            curIndex = charColours[i][1].index;
            length = 0;
        }
    }
}


} // to_utf8
