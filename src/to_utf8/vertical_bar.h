#pragma once

#include <string>


namespace to_utf8 {


void toVerticalBar(std::string& out, float perc);


} // to_utf8
