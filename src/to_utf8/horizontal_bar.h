#pragma once

#include <string>
#include <vector>


namespace to_utf8 {


void toHorizontalBar(
    std::string& out,
    const std::vector<uint64_t>& values,
    unsigned int width);


} // to_utf8

