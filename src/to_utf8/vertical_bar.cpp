#include "vertical_bar.h"

#include <cmath>
#include <array>


namespace to_utf8 {


const std::array<const std::string, 9> blocks {
    " ",
    "<span color='#3fff00'>▁</span>",
    "<span color='#7fff00'>▂</span>",
    "<span color='#bfff00'>▃</span>",
    "<span color='#ffff00'>▄</span>",
    "<span color='#ffbf00'>▅</span>",
    "<span color='#ff7f00'>▆</span>",
    "<span color='#ff3f00'>▇</span>",
    "<span color='#ff0000'>█</span>"
};

void toVerticalBar(std::string& out, float perc)
{
    if(perc < 0 || std::isnan(perc))
        perc = 0;
    if(perc > 100)
        perc = 100;

    int offset = floorf((perc + 6.25) / 12.5);

    out += blocks[offset];
}


} // to_utf8
