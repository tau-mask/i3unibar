#pragma once

#include <algorithm>
#include <array>
#include <ostream>

#include "value_collector_if.h"


namespace rend {


template<typename Type, std::size_t Size>
class Last
{
public:
    Last() {}

    void add(Type value) {
        m_values[m_index++] = value;
        if(m_index == Size)
            m_index = 0;
    }

    Type max() const {
        return *std::max_element(m_values.begin(), m_values.end());
    }

private:
    std::array<Type, Size> m_values = { 0 };
    std::size_t m_index = 0;
};


class InfoRenderer
{
public:
    InfoRenderer(bool needsPango) : m_needsPango(needsPango) {}
    virtual ~InfoRenderer() {}

    std::string render(ValueCollector& valueCollector);

protected:
    virtual void render(
        std::string& out,
        ValueCollector& valueCollector
    ) const = 0;

private:
    Last<std::size_t, 10> m_last;
    bool m_needsPango;
};

using InfoPrinter = std::tuple<InfoRenderer&, ValueCollector&>;

using UniqueRenderer = std::unique_ptr<InfoRenderer>;


} // rend


std::ostream& operator<<(std::ostream& out, rend::InfoPrinter infoPrinter);
