#include "renderer.h"


namespace rend {


std::string InfoRenderer::render(ValueCollector& valueCollector)
{
    std::string res;
    res.reserve(m_last.max());

    res += "{\"full_text\":\"";

    render(res, valueCollector);

    if(m_needsPango)
        res += "\",\"markup\":\"pango";
    res += "\"}";

    m_last.add(res.size());
    return res;
}


} // rend


std::ostream& operator<<(std::ostream& out, rend::InfoPrinter infoPrinter)
{
    out << std::get<0>(infoPrinter).render(std::get<1>(infoPrinter));
    return out;
}
