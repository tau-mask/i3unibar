#pragma once

#include <string>
#include <sys/statvfs.h>


namespace linux_ {


class StatFs
{
public:
    StatFs(const std::string& path);

    const std::string& getPath() const      { return m_path; }
    uint64_t getSizeFree() const            { return m_statvfs.f_bfree
                                                   * m_statvfs.f_frsize; }
    uint64_t getSizeTotal() const           { return m_statvfs.f_blocks
                                                   * m_statvfs.f_frsize; }

private:
    const std::string m_path;
    struct statvfs m_statvfs;
};


} // linux_
