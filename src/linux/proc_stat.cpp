#include "proc_stat.h"

#include <cstdio>
#include <cinttypes>
#include <numeric>

#include "../util/read_file.h"
#include "../util/string.h"


namespace linux_ {


ProcStat::ProcStat()
{
    std::string ps = util::readWholeFile ("/proc/stat");
    for(auto& line: util::split(ps, '\n')) {
        if(line.compare(0, 3, "cpu") == 0)
            m_cpus.push_back(line);
    }
}

std::uint64_t ProcStat::getCpuIdle(int cpuNumber) const
{
    return m_cpuStats[cpuNumber][TimeType::idle];
}

std::uint64_t ProcStat::getCpuTotal(int cpuNumber) const
{
    return std::accumulate(m_cpuStats[cpuNumber].cbegin(),
                           m_cpuStats[cpuNumber].cend(),
                           0);
}

std::size_t ProcStat::getCpuCount() const
{
    return m_cpuStats.size() - 1;
}


void ProcStat::parseCpus()
{
    if(!m_cpuStats.empty())
        return;

    m_cpuStats.resize(m_cpus.size());

    for(auto& cpuLine: m_cpus) {
        auto space = cpuLine.find(' ');
        auto sindex = cpuLine.substr(3, space - 3);
        size_t index = sindex.empty() ? 0 : std::stoi(sindex) + 1;

        if(index >= m_cpuStats.size())
            m_cpuStats.resize(index + 1);

        std::sscanf(
            cpuLine.c_str(),
            "%*s %" SCNu64 " %" SCNu64 " %" SCNu64 " %" SCNu64 " %" SCNu64
               " %" SCNu64 " %" SCNu64 " %" SCNu64 " %" SCNu64 " %" SCNu64,
            &m_cpuStats[index][TimeType::user],
            &m_cpuStats[index][TimeType::nice],
            &m_cpuStats[index][TimeType::system],
            &m_cpuStats[index][TimeType::idle],
            &m_cpuStats[index][TimeType::iowait],
            &m_cpuStats[index][TimeType::irq],
            &m_cpuStats[index][TimeType::softirq],
            &m_cpuStats[index][TimeType::steal],
            &m_cpuStats[index][TimeType::guest],
            &m_cpuStats[index][TimeType::guest_nice]
        );
    }
}


} // linux_
