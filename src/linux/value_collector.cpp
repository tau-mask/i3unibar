#include "value_collector.h"


namespace linux_ {


void ValueCollectorLinux::prepareCpuUsage()
{
    m_previous->parseProcStat();
    m_previous->m_ProcStat->parseCpus();

    parseProcStat();
    m_ProcStat->parseCpus();
}


float ValueCollectorLinux::getCpuUsage(int cpuNumber) const
{
    auto deltaIdle = m_ProcStat->getCpuIdle(cpuNumber) -
                     m_previous->m_ProcStat->getCpuIdle(cpuNumber);

    auto deltaTotal = m_ProcStat->getCpuTotal(cpuNumber) -
                      m_previous->m_ProcStat->getCpuTotal(cpuNumber);

    return static_cast<float>(deltaTotal - deltaIdle) / deltaTotal * 100;
}

std::vector<float> ValueCollectorLinux::getAllCpuUsage() const
{
    std::vector<float> values;

    for(std::size_t i = 0; i < m_ProcStat->getCpuCount(); ++i) {
        auto deltaIdle = m_ProcStat->getCpuIdle(i) -
                         m_previous->m_ProcStat->getCpuIdle(i);

        auto deltaTotal = m_ProcStat->getCpuTotal(i) -
                          m_previous->m_ProcStat->getCpuTotal(i);

        float perc = static_cast<float>(deltaTotal - deltaIdle) /
                     deltaTotal * 100;
        values.push_back(perc);
    }

    return values;
}

void ValueCollectorLinux::prepareMemUsage()
{
    if(!m_ProcMeminfo)
        m_ProcMeminfo.reset(new ProcMeminfo());
}

uint64_t ValueCollectorLinux::getMemTotal()
{
    return m_ProcMeminfo->getTotal();
}

uint64_t ValueCollectorLinux::getMemUsed()
{
    return m_ProcMeminfo->getTotal() - m_ProcMeminfo->getFree()
                                     - m_ProcMeminfo->getBuffCache();
}

uint64_t ValueCollectorLinux::getMemBuffCache()
{
    return m_ProcMeminfo->getBuffCache();
}

void ValueCollectorLinux::prepareNetUsage(
    const std::string& dev,
    vco::NetDetail details)
{
    if(!m_NetInfo)
        m_NetInfo = std::make_unique<NetInfo>();

    m_NetInfo->prepareDev(dev, details);
}

const std::string& ValueCollectorLinux::getDevIpAddr(const std::string& dev) const
{
    return m_NetInfo->getIpAddr(dev);
}

float ValueCollectorLinux::getDevLinkQuality(const std::string& dev) const
{
    auto max = m_NetInfo->getLinkMaxQuality(dev);
    if(max == 0)
        return 0.0f;

    return m_NetInfo->getLinkQuality(dev) / static_cast<float>(max) * 100;
}

void ValueCollectorLinux::preparePartUsage(const std::string& path)
{
    m_StatFs.push_back(std::make_unique<StatFs>(path));
}

uint64_t ValueCollectorLinux::getPartUsed(const std::string& path) const
{
    for(const auto& statfs: m_StatFs)
        if(statfs->getPath() == path)
            return statfs->getSizeTotal() - statfs->getSizeFree();

    return 0;   // TODO error handling
}

uint64_t ValueCollectorLinux::getPartTotal(const std::string& path) const
{
    for(const auto& statfs: m_StatFs)
        if(statfs->getPath() == path)
            return statfs->getSizeTotal();

    return 0;   // TODO error handling
}

void ValueCollectorLinux::parseProcStat()
{
    if(!m_ProcStat)
        m_ProcStat.reset(new ProcStat());
}


} // linux_
