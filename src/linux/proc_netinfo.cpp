#include "proc_netinfo.h"

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <iwlib.h>
#include <linux/if_link.h>
#include <netdb.h>
#include <sys/types.h>

#include "../util/read_file.h"
#include "../util/string.h"


namespace linux_ {


NetInfo::NetInfo()
{
    struct ifaddrs *ifap;
    if(getifaddrs(&ifap) == -1)
        return;                     // TODO error handling

    for(auto ifa = ifap; ifa != NULL; ifa = ifa->ifa_next) {
        if(ifa->ifa_addr == NULL)
            continue;

        auto family = ifa->ifa_addr->sa_family;

        if(family == AF_INET || family == AF_INET6) {
            char host[NI_MAXHOST];
            int len = (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                            sizeof(struct sockaddr_in6);
            int s = getnameinfo(
                ifa->ifa_addr, len,
                host, NI_MAXHOST,
                NULL, 0,
                NI_NUMERICHOST
            );
            if (s != 0)
                continue;           // TODO error handling

            if(family == AF_INET)
                m_devStats[ifa->ifa_name].ipv4 = host;
            else
                m_devStats[ifa->ifa_name].ipv6 = host;
        }
        else if(family == AF_PACKET && ifa->ifa_data != NULL) {
            auto stats = static_cast<struct rtnl_link_stats*>(ifa->ifa_data);

            auto& devStat = m_devStats[ifa->ifa_name];
            devStat.rx[DevStatType::packets] = stats->rx_packets;
            devStat.rx[DevStatType::bytes] = stats->rx_bytes;
            devStat.tx[DevStatType::packets] = stats->tx_packets;
            devStat.tx[DevStatType::bytes] = stats->tx_bytes;
        }
    }

    freeifaddrs(ifap);
}

void NetInfo::prepareDev(const std::string& dev, vco::NetDetail details)
{
    if(details & vco::NetDetail::Wifi)
        prepareWlanStats(dev);
}

const std::string& NetInfo::getIpAddr(const std::string& dev) const
{
    auto devStatEntry = m_devStats.find(dev);
    if(devStatEntry == m_devStats.end())
        return unknown;

    auto& devStat = devStatEntry->second;

    const std::string* connected = nullptr;
    const std::string* linkLocal = nullptr;

    if(!devStat.ipv6.empty()) {
        if(devStat.ipv6.compare(0, linkLocalIpv6.length(), linkLocalIpv6) == 0)
            linkLocal = &devStat.ipv6;
        else
            connected = &devStat.ipv6;
    }

    if(!devStat.ipv4.empty()) {
        if(devStat.ipv4.compare(0, linkLocalIpv4.length(), linkLocalIpv4) == 0)
            linkLocal = &devStat.ipv4;
        else
            connected = &devStat.ipv4;
    }

    if(connected)
        return *connected;
    if(linkLocal)
        return *linkLocal;

    return noAddress;
}

uint8_t NetInfo::getLinkQuality(const std::string& dev) const
{
    auto wStatEntry = m_wlanStats.find(dev);
    if(wStatEntry == m_wlanStats.end())
        return 0;

    return wStatEntry->second.linkQuality;
}

uint8_t NetInfo::getLinkMaxQuality(const std::string& dev) const
{
    auto wStatEntry = m_wlanStats.find(dev);
    if(wStatEntry == m_wlanStats.end())
        return 0;

    return wStatEntry->second.linkMaxQuality;
}

// From linux.cc in conky
void NetInfo::prepareWlanStats(const std::string& dev)
{
    struct wireless_info winfo;
    memset(&winfo, 0, sizeof(struct wireless_info));

    const char* s = dev.c_str();

    WlanStat wStat;

    int skfd = iw_sockets_open();
    if (iw_get_basic_config(skfd, s, &(winfo.b)) > -1) {
        // set present winfo variables
        if (iw_get_range_info(skfd, s, &(winfo.range)) >= 0) {
            winfo.has_range = 1;
        }
        if (iw_get_stats(skfd, s, &(winfo.stats), &winfo.range,
                         winfo.has_range) >= 0) {
            winfo.has_stats = 1;
        }

        // get bitrate
        struct iwreq wrq;
        if (iw_get_ext(skfd, s, SIOCGIWRATE, &wrq) >= 0) {
            memcpy(&(winfo.bitrate), &(wrq.u.bitrate), sizeof(iwparam));
            wStat.bitRate = winfo.bitrate.value;
        }

        // get link quality
        if( winfo.has_range &&
            winfo.has_stats &&
           (winfo.stats.qual.level != 0 || winfo.stats.qual.updated & IW_QUAL_DBM) &&
          !(winfo.stats.qual.updated & IW_QUAL_QUAL_INVALID) &&
            winfo.range.max_qual.qual > 0)
        {
            wStat.linkQuality = winfo.stats.qual.qual;
            wStat.linkMaxQuality = winfo.range.max_qual.qual;
        }

        // get essid
        if(winfo.b.has_essid)
            wStat.essid = (winfo.b.essid_on) ? winfo.b.essid : "";

    }

    iw_sockets_close(skfd);

    m_wlanStats[dev] = wStat;
}

const std::string NetInfo::empty = "";
const std::string NetInfo::unknown = "Unknown";
const std::string NetInfo::noAddress = "No Address";

const std::string NetInfo::linkLocalIpv4 = "169.254.";
const std::string NetInfo::linkLocalIpv6 = "fe80";

} // linux_

