#pragma once

#include <string>
#include <vector>
#include <cstdint>
#include <array>


namespace linux_ {


class ProcMeminfo
{
public:
    ProcMeminfo();

    uint64_t getTotal() const;
    uint64_t getBuffCache() const;
    uint64_t getFree() const;

private:
    static uint64_t unitMultiplier(std::array<char, 4> unit);

    std::vector<uint64_t> m_memInfo;
};


} // linux_

