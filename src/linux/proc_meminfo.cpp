#include "proc_meminfo.h"

#include <cstdlib>
#include <cinttypes>
#include <cstring>

#include "../util/read_file.h"
#include "../util/string.h"


namespace linux_ {


enum Entry : std::size_t {
    MemTotal,
    MemFree,
    Buffers,
    Cached,
    Slab,
    EOL
};

struct EntryIndex {
  const std::string name;
  const Entry value;
};

const std::array<EntryIndex, Entry::EOL> entryIndices = { {
  { "MemTotal", Entry::MemTotal },
  { "MemFree", Entry::MemFree },
  { "Buffers", Entry::Buffers },
  { "Cached", Entry::Cached },
  { "Slab", Entry::Slab },
} };

static_assert(Entry::EOL == entryIndices.size(), "Problem in EntryIndex");

Entry findEntryIndex(const std::string& name) {
    for(std::size_t i = 0; i < entryIndices.size(); ++i)
        if(entryIndices[i].name == name)
            return static_cast<Entry>(i);

    return Entry::EOL;
}


ProcMeminfo::ProcMeminfo()
{
    m_memInfo.reserve(entryIndices.size());

    std::string pm = util::readWholeFile("/proc/meminfo");
    for(auto& line: util::splitView(pm, '\n')) {
        char name[64];
        int nameLength = -1;
        uint64_t value;
        std::array<char, 4> unit{'\0'};

        auto ret = std::sscanf(line.data(), "%63[^:]%n: %" SCNu64 " %2s",
                               name, &nameLength, &value, unit.data());
        if(ret == EOF || ret < 2)
            continue;

        auto i = findEntryIndex(std::string(name, nameLength));
        if(i == Entry::EOL)
            continue;

        value *= unitMultiplier(unit);
        m_memInfo[i] = value;

        if(m_memInfo.size() == entryIndices.size())
            break;
    }
}

uint64_t ProcMeminfo::getTotal() const
{
    return m_memInfo[Entry::MemTotal];
}

uint64_t ProcMeminfo::getBuffCache() const
{
    return m_memInfo[Entry::Buffers] +
           m_memInfo[Entry::Cached] +
           m_memInfo[Entry::Slab];
}

uint64_t ProcMeminfo::getFree() const
{
    return m_memInfo[Entry::MemFree];
}


uint32_t arrToUint(std::array<char, 4> array)
{
    return *reinterpret_cast<uint32_t*>(array.data());
}

uint64_t ProcMeminfo::unitMultiplier(std::array<char, 4> unit)
{
    uint32_t x = arrToUint(unit);

    if(x == 0 || x == arrToUint({'B', '\0', '\0', '\0'}))
        return 1;

    if(x == arrToUint({'k', 'B', '\0', '\0'}))
        return 1024;

    if(x == arrToUint({'M', 'B', '\0', '\0'}))
        return 1024 * 1024;

    return 0;   // TODO error handling
}


} // linux_
