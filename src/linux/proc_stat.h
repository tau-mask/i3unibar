#pragma once

#include <string>
#include <vector>
#include <cstdint>
#include <array>


namespace linux_ {


class ProcStat
{
public:
    ProcStat();

    void parseCpus();

    std::uint64_t getCpuTotal(int cpu_number) const;
    std::uint64_t getCpuIdle(int cpu_number) const;

    std::size_t getCpuCount() const;

    enum TimeType
    {
        user,
        nice,
        system,
        idle,
        iowait,
        irq,
        softirq,
        steal,
        guest,
        guest_nice,
        MAX
    };

private:
    using CpuStat = std::array<std::uint64_t, TimeType::MAX>;

    std::vector<std::string> m_cpus;
    std::vector<CpuStat> m_cpuStats;
};


} // linux_
