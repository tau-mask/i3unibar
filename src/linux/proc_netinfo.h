#pragma once

#include <array>
#include <string>
#include <sys/statvfs.h>
#include <unordered_map>
#include <vector>

#include "../value_collector_options.h"


namespace linux_ {


class NetInfo
{
public:
    NetInfo();

    void prepareDev(const std::string& dev, vco::NetDetail details);

    const std::string& getIpAddr(const std::string& dev) const;
    uint8_t getLinkQuality(const std::string& dev) const;
    uint8_t getLinkMaxQuality(const std::string& dev) const;

private:
    void prepareWlanStats(const std::string& dev);

    enum DevStatType {
        bytes,
        packets,
        MAX
    };

    struct DevStat {
        std::string ipv4;
        std::string ipv6;
        std::array<std::uint64_t, DevStatType::MAX> rx;
        std::array<std::uint64_t, DevStatType::MAX> tx;
    };

    std::unordered_map<std::string, DevStat> m_devStats;

    struct WlanStat {
        std::string essid;
        int32_t bitRate = -1;
        uint8_t linkQuality = 0;
        uint8_t linkMaxQuality = 0;
    };

    std::unordered_map<std::string, WlanStat> m_wlanStats;

    static const std::string empty;
    static const std::string unknown;
    static const std::string noAddress;

    static const std::string linkLocalIpv4;
    static const std::string linkLocalIpv6;
};


} // linux_
