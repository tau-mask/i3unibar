#pragma once

#include <memory>

#include "../value_collector_if.h"
#include "proc_meminfo.h"
#include "proc_netinfo.h"
#include "proc_stat.h"
#include "statfs.h"


namespace linux_ {


class ValueCollectorLinux: public ValueCollectorInterface<ValueCollectorLinux>
{
public:
    void prepareCpuUsage() final;
    float getCpuUsage(int cpuNumber) const final;
    std::vector<float> getAllCpuUsage() const final;

    void prepareMemUsage() final;
    uint64_t getMemTotal() final;
    uint64_t getMemUsed() final;
    uint64_t getMemBuffCache() final;

    void prepareNetUsage(const std::string& dev, vco::NetDetail details) final;
    const std::string& getDevIpAddr(const std::string& dev) const final;
    float getDevLinkQuality(const std::string& dev) const final;

    void preparePartUsage(const std::string& path) final;
    uint64_t getPartUsed(const std::string & path) const final;
    uint64_t getPartTotal(const std::string & path) const final;

private:
    void parseProcStat();

    std::unique_ptr<ProcStat> m_ProcStat;
    std::unique_ptr<ProcMeminfo> m_ProcMeminfo;
    std::unique_ptr<NetInfo> m_NetInfo;
    std::vector<std::unique_ptr<StatFs>> m_StatFs;
};


} // linux_


using ValueCollector = linux_::ValueCollectorLinux;
