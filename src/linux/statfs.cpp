#include "statfs.h"

#include <cstring>


namespace linux_ {


StatFs::StatFs(const std::string& path) :
    m_path(path)
{
    int res = statvfs(m_path.c_str(), &m_statvfs);

    if(res == -1)
        std::memset(&m_statvfs, 0, sizeof(struct statvfs)); // TODO error handling
}


} // linux_
